package transmission

import (
	"testing"
)

func TestVersion(t *testing.T) {
	v, err := Version()
	if err != nil {
		t.Fatal(err)
	}
	t.Log("The version is:", v)
}

func TestAdd(t *testing.T) {
	err := Add("17b112b1931314e5910f0e1deaa11530b1c04faf") // random non-sense, just to see if it gets added.
	if err != nil {
		t.Fatal(err)
	}
}
