# transmission

Package transmission is a wrapper around transmission-cli to communicate with the transmission torrent downloader client.

## Usage

You need to have [Transmission and the Transmission command line interface](https://transmissionbt.com/download/) installed on the same machine as you use this package, otherwise using this package would not make much sense. Probably the cli can also communicate with transmission clients running on another machine but this package has no support for that (yet), simply because I did not need it. Any merge request to add this is welcome.

## Known issues

This package works on my FreeBSD system and that is what I made it for. There is preliminary support for other systems but I did not test those.

