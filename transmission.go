package transmission

import (
	"fmt"
	"os/exec"
	"strings"
)

const host = "localhost" // Maybe in the future add support for other hosts.

// Version returns the transmission-cli version. Can be used to test the integration.
func Version() (string, error) {
	args := make([]string, 0)
	args = append(args, "-V")
	version, err := execute(args)
	if err != nil {
		return "", err
	}
	if !strings.Contains(version, ".") {
		return "", fmt.Errorf("did not get version")
	}
	before := version[:strings.LastIndex(version, ".")]
	space := strings.LastIndex(before, " ")
	return version[space+1:], nil
}

// Add adds the provided torrent hash (magnet) to the transmission torrent download client.
func Add(hash string) error {
	if len(hash) != 40 {
		return fmt.Errorf("hash should be 40 characters long")
	}
	magnet := "magnet:?xt=urn:btih:" + hash
	args := make([]string, 0)
	args = append(args, "-a", magnet)
	_, err := execute(args)
	return err
}

func execute(args []string) (string, error) {
	cmdArgs := make([]string, 0)
	cmdArgs = append(cmdArgs, host)
	cmdArgs = append(cmdArgs, args...)
	out, err := exec.Command(cmd, cmdArgs...).CombinedOutput()
	if err != nil {
		return "", fmt.Errorf("cannot execute command '%v %v': %w", cmd, cmdArgs, err)
	}
	return string(out), nil
}
